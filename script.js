
console.log("working")

function CheckPassword(inputtxt) {
    if (inputtxt.length < 1) return
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (inputtxt.match(passw)) return
    else alert('Weak password!')
}

function CheckRadio() {
    var radios = document.getElementsByName('Radio');
    var i = 0;
    for (i = 0; i < radios.length; i++)
        if (radios[i].checked)
            return;
    alert("Please select a Role");
}

function CheckCheckBox() {
    var checks = document.getElementsByName('Check');
    var i = 0,j=0;
    for (i = 0; i < checks.length; i++)
    if (checks[i].checked) j++;
    if (j < 2) alert("Please select at least two Permissions ")
    else return
}

function onSubmit(event) {
    event.preventDefault()
    const passVal = document.getElementById("password").value
    CheckPassword(passVal)
    CheckRadio()
    CheckCheckBox()
}